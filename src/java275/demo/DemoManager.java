package java275.demo;

import java.util.Scanner;

/**
 * 
 * Demo管理器
 * 
 * @author leeson
 * 
 */

public class DemoManager {
	private static DemoManager _instance;

	public DemoManager() {

	}

	public static DemoManager getInstance() {
		if (_instance == null) {
			_instance = new DemoManager();
		}
		return _instance;
	}

	public void excuteDemo(DemoBase demo) {
		demo.excute();
	}

	public void disposeDemo(DemoBase demo) {
		demo.dispose();
	}
}
