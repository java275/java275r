package java275.demo.demoExcuter;

import java.util.*;
import java275.demo.DemoBase;

/**
 * 10/30 需求:輸入三個INT，並且取出最大值與最小值
 * 
 * 
 * @author leeson
 * 
 */
public class Demo20161030 implements DemoBase {
	// 此次執行的資料存取槽
	private StorgeBase storge;

	public Demo20161030() {

	}

	/**
	 * 
	 * @param inputNums
	 *            輸入次數
	 */
	public void excute(int inputNums) {

		if (storge == null) {
			storge = new StorgeInt();
		}
		System.out.println("-----DemoStart-----");
		// 依據inputNums來決定輸入次數
		while (ScannerManager.getInstance().getInputNums() < inputNums) {

			// 輸入狀態時鎖住
			if (!ScannerManager.getInstance().isRunning()) {
				try {
					System.out.println("請輸入第"
							+ (ScannerManager.getInstance().getInputNums() + 1)
							+ "個數字:");
					ScannerManager.getInstance().excute(storge);
				} catch (Exception e) {
					System.out.println(e.getLocalizedMessage());
					System.out.println("發生錯誤，請重新輸入");
				}
			}
		}
		System.out.println("最大值" + ((StorgeInt) storge)._getMaxNumber());
		System.out.println("最小值" + ((StorgeInt) storge)._getMinNumber());
		// 執行完畢清空資料
		((StorgeInt) storge)._clearData();
		// 釋放輸入實體
		ScannerManager.getInstance().dispose();
		// 再從頭開始
		excute(inputNums);
	}

	@Override
	public void excute() {
		excute(3);

	}

	@Override
	public void dispose() {
		ScannerManager.getInstance().dispose();
		((StorgeInt) storge)._clearData();
		storge = null;
	}
}

/**
 * 輸入工具
 * 
 * @author leeson
 * 
 */
class ScannerManager {
	private static ScannerManager _instance;
	private static Scanner _sc;
	private int _inputNums;
	private Boolean _isRunning;

	public ScannerManager() {
		_isRunning = false;
	}

	public static ScannerManager getInstance() {
		if (_instance == null) {
			_instance = new ScannerManager();
		}
		return _instance;
	}

	/**
	 * 是否正在執行中
	 * 
	 * @return
	 */
	public Boolean isRunning() {
		return _isRunning;
	}

	/**
	 * 取得目前輸入次數
	 */
	public int getInputNums() {
		return _inputNums;
	}

	/**
	 * 執行輸入，並將資料放入對應資料庫
	 * 
	 * @param targetStorge
	 */
	public void excute(StorgeBase targetStorge) throws Exception {

		if (_sc == null) {
			_sc = new Scanner(System.in);
		}

		_isRunning = true;

		// 判斷目標存取槽的型態
		if (targetStorge instanceof StorgeInt) {
			((StorgeInt) targetStorge)._addData(_sc.nextInt());
			_inputNums++;
		} else if (targetStorge instanceof StorgeFloat) {
			((StorgeFloat) targetStorge)._addData(_sc.nextFloat());
			_inputNums++;
		}
		_isRunning = false;
	}

	/**
	 * 釋放資源
	 */
	public void dispose() {
		// TODO 無法實作SCANNER CLOSE部分
		// _sc.close();
		// _sc = null;

		_isRunning = false;
		_inputNums = 0;
	}

}

/**
 * 資料存放基底
 * 
 * @author leeson
 * 
 */
class StorgeBase {

}

/**
 * float資料庫
 * 
 * @author leeson
 * 
 */
class StorgeFloat extends StorgeBase {

	private float[] _datas;
	private float _maxFloatDate;
	private float _minFloatData;

	public StorgeFloat() {
		_clearData();
	}

	/**
	 * 清空資料
	 */
	public void _clearData() {
		// 回復預設值
		_maxFloatDate = 0;
		_minFloatData = 0;
		_datas = null;
	}

	/**
	 * 增加資料
	 * 
	 * @param data
	 */
	public void _addData(float data) {
		float[] tmpData;
		if (_datas != null) {
			tmpData = new float[(_datas.length + 1)];// 動態增加長度
			// 將之前資料補齊
			for (int t = 0; t < _datas.length; t++) {
				tmpData[t] = _datas[t];
			}
			// 將最後資料補上
			tmpData[tmpData.length - 1] = data;
		} else {
			// 初始化
			tmpData = new float[1];
			tmpData[0] = data;
		}
		_datas = tmpData;
	}

	/**
	 * 取得最大值
	 * 
	 * @return
	 */
	public float _getMaxNumber() {
		if (_datas != null) {
			// 第一個數字當作基準點，避免直接用_maxIntDate判斷，預設值會汙染陣列
			_maxFloatDate = _datas[0];
			for (int m = 1; m < _datas.length; m++) {
				if (_datas[m] > _maxFloatDate) {
					_maxFloatDate = _datas[m];
				}
			}
		}
		return _maxFloatDate;
	}

	/**
	 * 取得最小值
	 * 
	 * @return
	 */
	public float _getMinNumber() {
		if (_datas != null) {
			// 第一個數字當作基準點，避免直接用_minIntData判斷，預設值會汙染陣列
			_minFloatData = _datas[0];
			for (int n = 1; n < _datas.length; n++) {
				if (_datas[n] < _minFloatData) {
					_minFloatData = _datas[n];
				}
			}
		}
		return _minFloatData;
	}
}

/**
 * Int資料庫
 * 
 * @author leeson
 * 
 */
class StorgeInt extends StorgeBase {

	private int[] _datas;
	private int _maxIntDate;
	private int _minIntData;

	public StorgeInt() {
		_clearData();
	}

	/**
	 * 清空資料
	 */
	public void _clearData() {
		// 回復預設值
		_maxIntDate = 0;
		_minIntData = 0;
		_datas = null;
	}

	/**
	 * 增加資料
	 * 
	 * @param data
	 */
	public void _addData(int data) {
		int[] tmpData;
		if (_datas != null) {
			tmpData = new int[(_datas.length + 1)];// 動態增加長度
			// 將之前資料補齊
			for (int t = 0; t < _datas.length; t++) {
				tmpData[t] = _datas[t];
			}
			// 將最後資料補上
			tmpData[tmpData.length - 1] = data;
		} else {
			// 初始化
			tmpData = new int[1];
			tmpData[0] = data;
		}
		_datas = tmpData;
	}

	/**
	 * 取得最大值
	 * 
	 * @return
	 */
	public int _getMaxNumber() {
		if (_datas != null) {
			// 第一個數字當作基準點，避免直接用_maxIntDate判斷，預設值會汙染陣列
			_maxIntDate = _datas[0];
			for (int m = 1; m < _datas.length; m++) {
				if (_datas[m] > _maxIntDate) {
					_maxIntDate = _datas[m];
				}
			}
		}
		return _maxIntDate;
	}

	/**
	 * 取得最小值
	 * 
	 * @return
	 */
	public int _getMinNumber() {
		if (_datas != null) {
			// 第一個數字當作基準點，避免直接用_minIntData判斷，預設值會汙染陣列
			_minIntData = _datas[0];
			for (int n = 1; n < _datas.length; n++) {
				if (_datas[n] < _minIntData) {
					_minIntData = _datas[n];
				}
			}
		}
		return _minIntData;
	}
}
