package java275.demo;

/**
 * Demo基底類
 * 
 * @author leeson
 * 
 */
public interface DemoBase {
	public void excute();
	public void dispose();
}
